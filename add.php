<div>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<?php
include_once 'js/hide.js';
include_once 'config/database.php';
include_once 'class/ProductTable.php';

$db = new Database();

$productTable = new ProductTable($db->getConnection());
$products = $productTable->GetList();
echo "<div class='addproduct-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center'>";
echo "<h1 class='display-4'>Add product page</h1>";
echo "<hr class='style1'>";
echo "</div>";


$categories = ProductFactory::GetCategories();
echo "<div class='form-group text-center row col-sm-2 mx-auto'>";
echo "<label class='col-text-center mx-auto'>Category</label>";
echo "<select id='category' class='form-control'>";

foreach ($categories as $category)
{
    echo "<option value='" . $category->id . "'>" . $category->name . "</option>";
}

echo "</select></div>";



foreach ($categories as $category)
{
    $product = $category->CreateProduct();
    $product->PrintAddForm();
}

?>


</html>