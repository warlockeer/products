<?php

class BookProduct extends Product 
{
    public $weight;
    protected function LoadAttributes($row)
    {
        $this->weight=$row['attr1'];
    }
    protected function SaveAttributes()
    {
        $attributes = [];
        $attributes['attr1'] = $this->weight;
        return $attributes;
    }

    protected function PrintAttributes()
    {
        echo "Book <br>";
        echo "Weight: " . $this->weight . " KG <br>";
    }

    protected function PrintAddFormAttributes()
    {
        echo "<label class='col-sm-2 col-form-label'>Weight</label>";
        echo "<input name='attr1' placeholder='Enter product weight in KG ' class='form-control'/>";
    }
}

?>