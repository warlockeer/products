<?php

include_once 'class/ProductFactory.php';

class ProductTable 
{
	private $conn;

 	public function __construct($conn)
 	{
    	$this->conn = $conn;
 	}

 	public function GetList()
 	{ 
 		$query = "SELECT * FROM products";
 		$stmt = $this->conn->prepare($query);
        $stmt->execute();
         $result=[];
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
         {
          $product = ProductFactory::CreateProduct($row['category_id']);
          $product->Load($row);
          array_push($result, $product);
         }
         return $result;
 	}
 	public function Create($product)
 	{ 
 	    $query = "INSERT INTO products SET sku=:sku, name=:name, price=:price, category_id=:category_id, attr1=:attr1, attr2=:attr2, attr3=:attr3";
 	    $stmt = $this->conn->prepare($query);


 	    $row = $product->Save();
 	    //var_dump($row);

 	    $stmt->bindParam(":sku", $row['sku']);
        $stmt->bindParam(":name", $row['name']);
        $stmt->bindParam(":price", $row['price']);
        $stmt->bindParam(":category_id", $row['category_id']);
        $stmt->bindParam(":attr1", $row['attr1']);
        $stmt->bindParam(":attr2", $row['attr2']);
        $stmt->bindParam(":attr3", $row['attr3']);

        if($stmt->execute()){
            return true;
        }else{
        	echo mysqli_error($this->$conn);
            return false;
        }
 	}
}
?>
