<?php

class DvdProduct extends Product 
{
    public $size;

    protected function LoadAttributes($row)
    {
        $this->size=$row['attr1'];
    }
    protected function SaveAttributes()
    {
        $attributes = [];
        $attributes['attr1'] = $this->size;
        return $attributes;
    }
    protected function PrintAttributes()
    {
        echo "Disc<br>";
        echo 'Size: ' . $this->size . ' MB <br>';
    }

    protected function PrintAddFormAttributes()
    {
        echo "<label class='col-sm-2 col-form-label'>Size</label>";
        echo "<input name='attr1' placeholder='Enter product size in MB ' class='form-control'/>";
    }
}

?>