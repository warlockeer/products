<?php

class FurnitureProduct extends Product 
{
    public $height;
    public $lenght;
    public $width;
    protected function LoadAttributes($row)
    {
        $this->height=$row['attr1'];
        $this->lenght=$row['attr2'];
        $this->width =$row['attr3'];
    }
    protected function SaveAttributes()
    {
        $attributes = [];
        $attributes['attr1'] = $this->height;
        $attributes['attr2'] = $this->lenght;
        $attributes['attr3'] = $this->width;
        return $attributes;
    }
    protected function PrintAttributes()
    {
        echo "Furniture <br>";
        echo "Dimensions: " . $this->height . " x " . $this->lenght . " x " . $this->width . " <br>";
    }

    protected function PrintAddFormAttributes()
    {
        echo "<label class='col-sm-2 col-form-label'>Width</label>";
        echo "<input name='attr3' placeholder='Enter product width in CM ' class='form-control'/>";
        echo "<label class='col-sm-2 col-form-label'>Height</label>";
        echo "<input name='attr1' placeholder='Enter product in height CM ' class='form-control'/>";
        echo "<label class='col-sm-2 col-form-label'>Length</label>";
        echo "<input name='attr2' placeholder='Enter product in length CM ' class='form-control'/>";
    }
}

?>