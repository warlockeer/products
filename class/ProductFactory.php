<?php 

include_once 'class/Product.php';
include_once 'class/DvdProduct.php';
include_once 'class/BookProduct.php';
include_once 'class/FurnitureProduct.php';

class ProductCategory
{
    public $id;
    public $name;
    public $class;

    function __construct($id, $name, $class)
    {
        $this->id = $id;
        $this->name = $name;
        $this->class = $class;
    }

    public function CreateProduct()
    {
        $result = new $this->class;
        $result->category_id = $this->id;
        return $result;
    }
}

class ProductFactory 
{
    public static function GetCategories()
    {
        $categories = [];
        ProductFactory::AddCategory($categories, new ProductCategory(1, "DVD", DvdProduct::class));
        ProductFactory::AddCategory($categories, new ProductCategory(2, "Book", BookProduct::class));
        ProductFactory::AddCategory($categories, new ProductCategory(3, "Furniture", FurnitureProduct::class));
        return $categories;
    }

    public static function CreateProduct($type)
    {
        $categories = ProductFactory::GetCategories();
        return $categories[$type]->CreateProduct();
    }

     private static function AddCategory(& $categories, $category)
     {
         $categories[$category->id] = $category;
     }
}
?>