<?php
abstract class Product 
{
    public $id;
    public $sku;
    public $name;
    public $price;
    public $category_id;

     public function Load($row)
     {
     	$this->id=$row['id'];
     	$this->sku=$row['sku'];
     	$this->name=$row['name'];
     	$this->price=$row['price'];
     	$this->category_id=$row['category_id'];
     	$this->LoadAttributes($row);
     }

     public function Save()
     {
     	$row = [];
     	$row['id'] = $this->id;
     	$row['sku'] = $this->sku;
     	$row['name'] = $this->name;
     	$row['price'] = $this->price;
     	$row['category_id'] = $this->category_id;

     	$attributes['attr1'] = 0;
     	$attributes['attr2'] = 0;
     	$attributes['attr3'] = 0;
     	
     	$attributes = array_merge($attributes, $this->SaveAttributes());

     	return array_merge($row, $attributes);
     }
     public function PrintElement()
     {

        echo "<div class='card text-white text-center bg-dark mb-3' style='max-width: 18rem;'>";
     	echo "<div class='card-header'>" . $this->sku . "</div>";
     	echo "<div class='card-body'>";
     	echo "<p class='card-text'>  $this->name  <br>";
     	echo $this->price . ' $ <br>';
     	$this->PrintAttributes();
     	echo "</p>";
     	echo "</div>";
        echo "</div>";

     }

    public function PrintAddForm()
    {
        echo "<div class='container' data-category-form='" . $this->category_id . "'>";
        echo "  <form action='action_add.php' method='post'>";
        echo "    <div class='form-group row col-sm-3 mx-auto'>";
        echo "      <label for='name' class='col-sm-2 col-form-label'>Name</label>";
        echo "      <input name='name' placeholder='Enter product name ' class='form-control' />";
        echo "      <label for='sku' class='col-sm-2 col-form-label'>SKU</label>";
        echo "      <input name='sku' placeholder='Enter product SKU code ' class='form-control'/>";
        echo "      <label for='sku' class='col-sm-2 col-form-label'>Price</label>";
        echo "      <input name='price' placeholder='Enter product price ' class='form-control'/>";

        $this->PrintAddFormAttributes();

        echo "    </div>";
        echo "    <div class='row'>";
        echo "      <div class='col text-center'>";
        echo "        <button type='submit' class='btn btn-primary btn-lg'>Submit</button>";
        echo "      </div>";
        echo "    </div>";
        echo "    <input type='hidden' name='category_id' value='" . $this->category_id . "' />";
        echo "  </form>";
        echo "</div>";
    }

    abstract protected function LoadAttributes($row);
    abstract protected function SaveAttributes();
    abstract protected function PrintAttributes();
    abstract protected function PrintAddFormAttributes();
}
?>