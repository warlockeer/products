<script>
$(function() {
    $("#category").change(function() {
        $('[data-category-form]').hide();
        $('[data-category-form=' + $(this).val() + ']').show();
    });

    $("#category").val(1).trigger('change');
});

</script>